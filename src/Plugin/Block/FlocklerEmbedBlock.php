<?php

namespace Drupal\flockler_embed_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'Flockler Embed' Block.
 *
 * @Block(
 *   id = "flockler_embed_block",
 *   admin_label = @Translation("Flockler Embed block"),
 *   category = @Translation("Custom"),
 * )
 */
class FlocklerEmbedBlock extends BlockBase implements BlockPluginInterface {
	/**
	 * {@inheritdoc}
	 */
	public function build() {
		$config = $this->getConfiguration();
		
		$block = [
			'#theme' => 'flockler_embed',
			'#site' => $config['site'],
			'#attached' => [
				'library' => [
					'flockler_embed_block/flockler_embed'
				]
			]
		];
		
		if (isset($config['section']))
			$block['#section'] = $config['section'];
		
		if (isset($config['count']))
			$block['#count'] = $config['count'];
		
		if (isset($config['refresh']))
			$block['#refresh'] = $config['refresh'];
		
		if (isset($config['refresh_type']))
			$block['#refresh_type'] = $config['refresh_type'];
		
		if (isset($config['default_css']))
			$block['#default_css'] = $config['default_css'];
		
		if (isset($config['default_js']))
			$block['#default_js'] = $config['default_js'];
		
		if (isset($config['infinite_scroll']))
			$block['#infinite_scroll'] = $config['infinite_scroll'];
		
		if (isset($config['infinite_scroll']))
			$block['#infinite_scroll'] = $config['infinite_scroll'];
		
		if (isset($config['articles']))
			$block['#articles'] = $config['articles'];
		
		if (isset($config['tags']))
			$block['#tags'] = "'" . str_replace(",", "','", $config['tags']) . "'";
		
		if (isset($config['filter_only']))		
			$block['#filter_only'] = $config['filter_only'];		
		
		if (isset($config['load_more_text_enabled'])) {
			$block['#load_more_text'] = false;
		} elseif (isset($config['load_more_text'])) {
			$block['#load_more_text'] = $config['load_more_text'];
		}
		
		if (isset($config['show_more_text']))
			$block['#show_more_text'] = $config['show_more_text'];
		
		return $block;
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function blockForm($form, FormStateInterface $form_state) {
		$form = parent::blockForm($form, $form_state);
		
		$config = $this->getConfiguration();
		
		$form['site'] = [
			'#type' => 'textfield',
			'#title' => t('Site ID'),
			'#description' => t('Flockler site ID.'),
			'#required' => true,
			'#pattern' => '^\d+$',
			'#default_value' => isset($config['site']) ? $config['site'] : ''
		];
		
		$form['section'] = [
			'#type' => 'textfield',
			'#title' => t('Section ID (Optional)'),
			'#description' => t("Flockler section ID."),
			'#pattern' => '^\d+$',
			'#default_value' => isset($config['section']) ? $config['section'] : ''
		];
		
		$form['count'] = [
			'#type' => 'number',
			'#title' => t('Count'),
			'#description' => t("How many items are shown \"per page\"."),
			'#default_value' => isset($config['count']) ? $config['count'] : 40
		];
		
		$form['refresh'] = [
			'#type' => 'select',
			'#title' => t('Refresh'),
			'#description' => t("Check for new content every given milliseconds."),
			'#options' => [
				0 => 'Disabled',
				10000 => '10sec',
				30000 => '30sec (Recommended)'
			],
			'#default_value' => isset($config['refresh']) ? $config['refresh'] : 30000
		];
		
		$form['refresh_type'] = [
			'#type' => 'select',
			'#title' => t('Refresh Type'),
			'#description' => t("What to do on new content; Auto = add new content to the top of stream automatically pushing the stream down, Manual = render a button at top so the stream doesn't jump."),
			'#options' => [
				'auto' => 'Auto',
				'manual' => 'Manual'
			],
			'#default_value' => isset($config['refresh_type']) ? $config['refresh_type'] : 'manual'
		];
		
		$form['default_css'] = [
			'#type' => 'select',
			'#title' => 'Default CSS',
			'#description' => t("Set to false to ignore embed style's CSS and e.g. use custom CSS."),
			'#options' => [
				'false' => 'False',
				'true' => 'True'
			],
			'#default_value' => isset($config['default_css']) ? $config['default_css'] : 'true'
		];
		
		$form['default_js'] = [
			'#type' => 'select',
			'#title' => 'Default JavaScript',
			'#description' => t("Set to false to ignore embed style's JavaScript and e.g. use custom JS."),
			'#options' => [
				'false' => 'False',
				'true' => 'True'
			],
			'#default_value' => isset($config['default_js']) ? $config['default_js'] : 'true'
		];
		
		$form['infinite_scroll'] = [
			'#type' => 'select',
			'#title' => 'Infinite Scroll',
			'#description' => t("Enables infinite scroll (disabled by default)."),
			'#options' => [
				'0' => 'False',
				'1' => 'True'
			],
			'#default_value' => isset($config['infinite_scroll']) ? $config['infinite_scroll'] : '0'
		];
		
		$form['articles'] = [
			'#type' => 'textfield',
			'#title' => t('Articles'),
			'#description' => t('Only include the specified articles in the stream.'),
			'#pattern' => '(\d+)(,)*\d',
			'#default_value' => isset($config['articles']) ? $config['articles'] : ''
		];
		
		$form['tags'] = [
			'#type' => 'textfield',
			'#title' => t('Tags'),
			'#description' => t('Filter articles based on a tag or multiple tags. With multiple tags filter is OR: one of the tags needs to match.'),
			'#pattern' => '(\w+)(,)*\w',
			'#default_value' => isset($config['tags']) ? $config['tags'] : ''
		];
		
		$form['filter_only'] = [
			'#type' => 'select',
			'#title' => 'Filter (Only)',
			'#description' => t("Filter articles by type "),
			'#options' => [
				'' => t('None'),
				'facebook' => t('Facebook'),
				'instagram' => t('Instagram'),
				'linkedin' => t('LinkedIn'),
				'pinterest' => t('Pinterest'),
				'twitter' => t('Twitter'),
				'videos' => t('Videos'),
				'video_posts' => t('Video Posts'),
				'yammer' => t('Yammer'),
				'youtube' => t('YouTube')
			],
			'#default_value' => isset($config['filter_only']) ? $config['filter_only'] : ''
		];
		
		$form['load_more_text_enabled'] = [
			'#type' => 'select',
			'#title' => 'Enable Load More Text',
			'#options' => [
				'false' => 'False',
				'true' => 'True'
			],
			'#default_value' => isset($config['load_more_text_enabled']) ? $config['load_more_text_enabled'] : 'true'
		];
		
		$form['load_more_text'] = [
			'#type' => 'textfield',
			'#title' => t('Load More Text'),
			'#description' => t('Change load more button text. Leave field blank to use default text.'),
			'#pattern' => '(\w)* (\w)*',
			'#default_value' => isset($config['load_more_text']) ? $config['load_more_text'] : '',
			'#states' => [
				'visible' => [
					':input[name="settings[load_more_text_enabled]"]' => array('value' => 'true'),
				],
			]
		];
		
		$form['show_more_text'] = [
			'#type' => 'textfield',
			'#title' => t('Show More Text'),
			'#description' => t('Change show more button text. Leave field blank to use default text.'),
			'#pattern' => '(\w)* (\w)*',
			'#default_value' => isset($config['show_more_text']) ? $config['show_more_text'] : '',
		];

		return $form;
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function validateForm(array &$form, FormStateInterface $form_state) {
	}
  
	/**
	  * {@inheritdoc}
	  */
	public function blockSubmit($form, FormStateInterface $form_state) {
		parent::blockSubmit($form, $form_state);
		
		$values = $form_state->getValues();
		
		$config = $this->configuration;
		
		$config['site'] = $values['site'];
		
		if ($values['section']) {
			$config['section'] = $values['section'];
		} else {
			unset($config['section']);
		}
		
		if ($values['count'] !=40) { /* default */
			$config['count'] = $values['count'];
		} else {
			unset($config['count']);
		}
		
		/**
		 * Optional, however API documentation does not specify default.
		 * @see https://developers.flockler.com/embed/
		 **/
		$config['refresh'] = $values['refresh'];
		
		if ($values['refresh_type'] != "manual") { 
			$config['refresh_type'] = $values['refresh_type'];
		} else { /* default (manual) */
			unset($config['refresh_type']);
		}
		
		if (!$values['default_css']) {
			$config['default_css'] = $values['default_css'];
		} else { /* default (true) */
			unset($config['default_css']);
		}
		
		if (!$values['default_js']) {
			$config['default_css'] = $values['default_js'];
		} else { /* default (true) */
			unset($config['default_js']);
		}
		
		if ($values['infinite_scroll']) {
			$config['infinite_scroll'] = $values['infinite_scroll'];
		} else { /* default (0) */
			unset($config['infinite_scrol']);
		}
		
		if ($values['articles']) {
			$config['articles'] = $values['articles'];
		} else {
			unset($config['articles']);
		}
		
		if ($values['tags']) {
			$config['tags'] = $values['tags'];
		} else {
			unset($config['tags']);
		}
		
		if (!empty($values['filter_only'])) {
			$config['filter_only'] = $values['filter_only'];
		} else { /* default */
			unset($config['tags']);
		}
		
		if (!$values['load_more_text_enabled']) {
			$config['load_more_text_enabled'] = $values['load_more_text_enabled'];
		} else { /* default (true) */
			unset($config['load_more_text_enabled']);
		}
		
		if (!empty($values['load_more_text']) && $values['load_more_text_enabled']) {
			$config['load_more_text'] = $values['load_more_text'];
		} else { /* default */
			unset($config['load_more_text']);
		}

		if (!empty($values['show_more_text'])) {
			$config['show_more_text'] = $values['show_more_text'];
		} else { /* default */
			unset($config['show_more_text']);
		}
		
		$this->configuration = $config;
	}
}